$('#sendEmail').click(function(ev){
    // Disabled send button once clicked.
    $("#sendEmail").attr("disabled", true);
    var params={};
    params['site']= $('#permissionSite').val();
    params['teacherEmail']= $('#teacherEmail').val();
    params['reason']= $('#questionSelection').val();
    params['otherReason']= $('#otherReasonInfo').val();
    params['categories'] = "Chat/Messaging, Games";
    params['keyword'] = "";
    params['blockedReason'] = "Blocked by Category";
    params['policy'] = "Middle+School";
    params['requesterOU'] = "redacted";
    params['requesterSafeSecGroupName'] = "-";
    params['requester'] = "redacted@redacted.com";
    params['fid'] = "redacted@redacted.redated.redacted.us";
    params['i2n'] = "1104006318";
    params['dateTime'] = "Wednesday, 09/14/22, 09:18AM PDT";
    params['externalIp'] = "66.154.221.90";
    params['ua'] = "Other";
    params['filteringMethod'] = "SmartPac";
    params['url'] = "discord.com"

    if (showTWL) {
        sendEmail(params, 'teacher');
    } else {
        sendEmail(params, 'admin');
    }
    ev.preventDefault();
});
https://www.securly.com/app/api/sendtwl
                params['fid'] = 
