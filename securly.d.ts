export function loginAs(emailAddress: string):Promise<Securly>

interface Securly {
    getURLBlockingStatus(url:string):Promise<{
        categories:("safe_search"|"safe_youtube"|"blacklist"|"not_whitelisted"|"safe_url"|"pornography"|"drugs"|"gambling"|"adult_content"|"social_media"|"anonymous_proxy"|"online_chatting"|"web_mail"|"hate_speech"|"other_search_engines"|"social_media"|"web_advertisments"|"live_streams"|"games"|"health"|"educational_youtube")[],
        blockState: "allowed"|"blocked"|"paused"
        categoryInteger:number,
        blackListed: boolean,
        blackListType: "global"|"policy"|"whitelist_only"|null,
        blackListMode: "blocklist"|"whitelist"
    }>
    getYouTubeSettings():Promise<{
        hideComments:boolean,
        hideSidebar:boolean,
        hideThumbnails:boolean
    }>
    getConfiguration():Promise<{
        reloadTabs: boolean,
        success: boolean,
        thinkTwice: boolean
    }>
}
