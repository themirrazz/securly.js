# Securly.JS

Get YouTube settings and blocklist from Securly!

Example:
```js
const Securly=require("./securly");
Securly.loginAs("yourschoolemail@yourschooldomain.edu").then(async (account) => {
    var urls=["discord.com","google.com","wikipedia.com","youtube.com","reddit.com","instagram.com","twitter.com","bing.com",
    "duckduckgo.com","scratch.mit.edu","gitlab.com","microsoft.com","github.com","spotify.com","netflix.com","open.spotify.com"];
    for(var i=0;i<urls.length;i++) {
        console.log(
            `${urls[i]} is ${(await account.getURLBlockingStatus(`https://${urls[i]}/`)).blockState==="allowed"?"not blocked":"blocked"}`
        );
    }
})
```
