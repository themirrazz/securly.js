const https=require('https');
const cheerio=require("cheerio");

let _securly_external = {
    loginAs:function(email) {
        return new Promise((resolve,reject)=>{
            const req=https.request(`https://www.securly.com/crextn/cluster?domain=${email.split("@")[1]}&reasonCode=0&version=2.42`,{
                headers: {
                    'user-agent':"Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                }
            },function (res) {
                var data="";
                res.on('data', (chunk) => {
                    data+=chunk
                });
                res.on('end',()=>{
                    try {
                        new URL(data);
                        resolve(new Securly(
                            email,data
                        ))
                    } catch (error) {
                        if(data==="UNKNOWN_SCHOOL") {
                            reject(new TypeError("invalid school"))
                        } else {
                            reject(new TypeError("invalid server response"));
                        }
                    }
                })
            });
            req.onerror=()=>reject(new Error("failed to get cluster URL"));
            req.end();
        })
    }
}

class Securly {
    #email="@";
    #cluster="https://www.securly.com/crextn";
    constructor(emailAddress, clusterURL) {
        this.#email=emailAddress,
        this.#cluster=clusterURL;
    }
    getConfiguration() {
        return new Promise((o,e)=>{
            var req=https.get(
                `${this.#cluster}/config`,
                {
                    headers: {
                        'user-agent': "Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                    }
                },
                (res)=>{
                    var data="";
                    res.on('data', (chunk) => {
                        data+=chunk
                    });
                    res.on('end', ()=>{
                        try {
                            var d=JSON.parse(data);
                            o({
                                reloadTabs:d.reload_tabs===1,
                                success: d.success===1,
                                thinkTwice: d.thinkTwice===1
                            });
                        } catch (error) {
                            e(new TypeError("invalid server response"))
                        }
                    })
                }
            );
            req.onerror=()=>e();
            req.end();
        });
    }
    getURLBlockingStatus(url) {
        return new Promise((o,e)=>{
            var req=https.get(`${this.#cluster}/broker?&reason=crextn&useremail=${this.#email}&url=${encodeURIComponent(url)}&host=${encodeURIComponent(new URL(url).hostname)}`,{
                headers: {
                    'user-agent':"Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                }
            },(res) => {
                var data="";
                res.on('data', (chunk) => {
                    data+=chunk
                });
                res.on('end',()=>{
                    data=data.slice(1);
                    var z=data.split(":");
                    var status={
                        blockState: "allowed",
                        categories:[],
                        categoryInteger: 0,
                        blackListed: false,
                        blackListType: null,
                        blockingMode: "blocklist",
                        banned: false,
                        safeSearch: false,
                        policyId: z[1]
                    }
                    if(z[3]!="-1") { z.safeSearch=true }
                    if(z[2]!="-1"&&!isNaN(z[2])) {
                        status.categories=getCategories(z[2]);
                        status.categoryInteger=Number(z[2]);
                    }
                    if(z[2]==="BANNED") {
                        status.categories.push("banned");
                        status.banned=true;
                    }
                    if(z[2]==="WL"||z[2]==="WL_SRCH") {
                        status.categories.push("not_whitelisted");
                        status.blackListed=true;
                        status.blackListType="whitelist_only";
                        status.blockingMode="whitelist";
                    }
                    if(z[2]==="BL"||z[2]==="BL_SRCH") {
                        status.categories.push("blacklist");
                        status.blackListed=true;
                        if(z[1]==="G") {
                            status.blackListType="global"
                        } else {
                            status.blackListType="policy"
                        }
                    }
                    if(z[0]==="DENY") {
                        status.blockState="blocked"
                    } else if(z[0]==="PAUSE") {
                        status.blockState="paused"
                    }
                    o(status);
                })
            });
            req.onerror=()=>{e(new Error("failed to fetch blocking status"))}
            req.end();
        });
    }
    getURLBlockingStatus(url) {
        return new Promise((o,e)=>{
            var req=https.get(`${this.#cluster}/broker?&reason=crextn&useremail=${this.#email}&url=${encodeURIComponent(url)}&host=${encodeURIComponent(new URL(url).hostname)}`,{
                headers: {
                    'user-agent':"Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                }
            },(res) => {
                var data="";
                res.on('data', (chunk) => {
                    data+=chunk
                });
                res.on('end',()=>{
                    data=data.slice(1);
                    var z=data.split(":");
                    var status={
                        blockState: "allowed",
                        categories:[],
                        categoryInteger: 0,
                        blackListed: false,
                        blackListType: null,
                        blockingMode: "blocklist",
                        banned: false,
                        safeSearch: false,
                        policyId: z[1]
                    }
                    if(z[3]!="-1") { z.safeSearch=true }
                    if(z[2]!="-1"&&!isNaN(z[2])) {
                        status.categories=getCategories(z[2]);
                        status.categoryInteger=Number(z[2]);
                    }
                    if(z[2]==="BANNED") {
                        status.categories.push("banned");
                        status.banned=true;
                    }
                    if(z[2]==="WL"||z[2]==="WL_SRCH") {
                        status.categories.push("not_whitelisted");
                        status.blackListed=true;
                        status.blackListType="whitelist_only";
                        status.blockingMode="whitelist";
                    }
                    if(z[2]==="BL"||z[2]==="BL_SRCH") {
                        status.categories.push("blacklist");
                        status.blackListed=true;
                        if(z[1]==="G") {
                            status.blackListType="global"
                        } else {
                            status.blackListType="policy"
                        }
                    }
                    if(z[0]==="DENY") {
                        status.blockState="blocked"
                    } else if(z[0]==="PAUSE") {
                        status.blockState="paused"
                    }
                    o(status);
                })
            });
            req.onerror=()=>{e(new Error("failed to fetch blocking status"))}
            req.end();
        });
    }
    getRequestUnblock (url,teacherEmail,reason,customReason) {
        var g=""
        switch (reason) {
            case 'custom':
            case 'other':
                g="Other";
                break
            case 'complete_assignment':
            default:
                g="Need access to complete home work/assignment/project";
                break
            case 'research':
                g="Need access to conduct research for a paper";
                break
            case 'study_material':
                g="Need access to use it as studying material in a class";
        }
        return new Promise(async (o,e)=>{
            var blockData=await this.getURLBlockingStatus(url);
            var page=await new Promise((w,q)=>{
                var u=https.get(
                    `https://www.securly.com/blocked?useremail=${
                        this.#email
                    }&reason=${
                        blockData.categories.includes("safe_search")?'safesearch':(
                            blockData.categories.includes("blocklist")||blockData.categories.includes("whitelist")?(
                                blockData.blackListType==="global"?"policyblacklist":"globalblacklist"
                            ):"blockedbycategory"
                        )
                    }&categoryid=${
                        blockData.blackListed?"BL":blockData.categoryInteger
                    }&ver=2.98.40&keyword=&url=${btoa(url)}`,
                    (p)=>{
                        var t='';
                        p.on('data',chunk=>{ t+=chunk })
                        p.on('end',()=>q(t))
                    }
                );
                u.onerror=()=>q(new Error("I.intfail"));
                u.end();
            });
            var req=https.get(
                "https://www.securly.com/app/api/sendtwl",
                {
                    method: 'POST',
                    headers: {
                        'user-agent': "Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                    }
                },
                (res)=>{o()}
            );
            var d=new Date()
            req.onerror=()=>e(new Error("failed to submit unblocking request"));
            req.write(JSON.stringify({
                site: new URL(url).host,
                url: new URL(url).host,
                teacherEmail: teacherEmail,
                reason:g,
                otherReason:customReason||"",
                keyword: "",
                blockedReason: 0,
                policy: getSendEmailParam(page,'policy'),
                categories: getSendEmailParam(page,'categories'),
                requesterOU: getSendEmailParam(page,'requesterOU'),
                requesterSafeSecGroupName: getSendEmailParam(page,'requestSafeSecGroupName'),
                externalIP: getSendEmailParam(page,'externalIP'),
                ua: 'CrOS',
                requester: this.#email,
                fid: getSendEmailParam(page,'fid'),
                i2n: getSendEmailParam(page,'i2n'),
                dateTime: `${weekday[d.getDay()]} ${d.getMonth()+1}/${d.getDate()}/${d.getFullYear()} ${d.getHours()>12?d.getHours()-12:d.getHours()}:${d.getMinutes()}${d.getHours()>11?"AM":"PM"} XDT`
            }));
            req.end()
        })
    }
    getYouTubeSettings() {
        return new Promise((o,e)=>{
            var req=https.get(`${this.#cluster}/broker?&reason=crextn&useremail=${this.#email}&url=&ytoptions=1`,{
                headers: {
                    'user-agent':"Mozilla/5.0 (X11; CrOS x86_64 10066.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                }
            },(res) => {
                var data="";
                res.on('data', (chunk) => {
                    data+=chunk
                });
                res.on('end',()=>{
                    data=data.slice(1);
                    var z=data.split(":");
                    var status={
                        hideThumbnails: z[1]==="true",
                        hideComments: z[0]==="true",
                        hideSidebar: z[2]==="true"
                    }
                    o(status);
                })
            });
            req.onerror=()=>{e(new Error("failed to fetch blocking status"))}
            req.end();
        });
    }
}

var weekday=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']


function getCategories(number) {
    var categories=[
    "safe_search",
    "safe_youtube",
    "safe_url",
    "pornography",
    "drugs",
    "gambling",
    "adult_content",
    "social_media",
    "anonymous_proxy",
    "online_chatting",
    "web_mail",
    "hate_speech",
    "other_search_engines",
    "social_media",
    "web_advertisments",
    "live_streams",
    "games",
    "health",
    "educational_youtube"
];

function increaseBinary(b) {
    var o=0
    if(b>0){o=1}else{return o}
    for(var i=0;i<b;i++){o*=2}
    return o
}

function compareBinaryWithCategories(b) {
    for(var i=0;i<categories.length;i++) {
        if(increaseBinary(i+1)===b) {
            return categories[i]
        }
    }
    return null;
}

function getSendEmailParam(html,param) {
    var w=`                params['${
        JSON.stringify(param).slice(1,JSON.stringify(param).length-1)
    }'] =`;
    if(html.indexOf(w)<0){return null}
    var o= html.slice(
        html.indexOf(w)
    ).split("\n")[0].slice(w.length,html.slice(
        html.indexOf(w)
    ).split("\n")[0].indexOf('";')+1);
    try {
        return JSON.parse(o)
    } catch (q) {
        return null
    }
}


function reverseText(e){
var g="";
for(var i=e.length-1;i>-1;i--){g+=e[i]}
return g
}
    var place=(number >>> 0).toString(2);
    place=reverseText(place)
    var result=[];
    var kO;
    for(var i=0;i<place.length;i++) {
        kO=compareBinaryWithCategories(
            increaseBinary(i+1)
        );
        if(kO&&place[i]==="1") {
            result.push(kO)
        } 
    }
    return result
}

module.exports=_securly_external;
